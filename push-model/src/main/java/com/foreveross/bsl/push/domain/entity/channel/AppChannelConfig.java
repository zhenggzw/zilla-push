package com.foreveross.bsl.push.domain.entity.channel;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.foreveross.bsl.mongodb.domain.Entity;

@Document
public class AppChannelConfig extends Entity {

    @Id
	private String appId;
	
	@Field
	private Date createTime;
	
	@Field
	private String userId;
	
	@Field
	private ApnsConfig apnsConfig;
	
	@Field
	private ApnsConfig apnsSandboxConfig;
	
	@Field
	private OpenfireConfig openfireConfig;

	@Field
	private InstantConfig instantConfig; // add by hyw 20140519

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public ApnsConfig getApnsConfig() {
		return apnsConfig;
	}

	public void setApnsConfig(ApnsConfig apnsConfig) {
		this.apnsConfig = apnsConfig;
	}

	public OpenfireConfig getOpenfireConfig() {
		return openfireConfig;
	}

	public void setOpenfireConfig(OpenfireConfig openfireConfig) {
		this.openfireConfig = openfireConfig;
	}

	public InstantConfig getInstantConfig() {
		return instantConfig;
	}

	public void setInstantConfig(InstantConfig instantConfig) {
		this.instantConfig = instantConfig;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the apnsSandboxConfig
	 */
	public ApnsConfig getApnsSandboxConfig() {
		return apnsSandboxConfig;
	}

	/**
	 * @param apnsSandboxConfig the apnsSandboxConfig to set
	 */
	public void setApnsSandboxConfig(ApnsConfig apnsSandboxConfig) {
		this.apnsSandboxConfig = apnsSandboxConfig;
	}
	
}