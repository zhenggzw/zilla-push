/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.repository.custom;


/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-15
 * 
 */
public interface TagSummaryRepositoryCustom {
	/**
	 * 更新应用的标签摘要，如果标签摘要不存在则新增
	 * @param appId
	 * @param key
	 * @param tagValues
	 */
	void updateTags(String appId, String key, String... tagValues);
	
	/**
	 * 删除指定应用ID的标签键的标签值。如果标签值(即tagValues)为空，则删除该标签键的所有值，如果标签键和值都为空则删除该应用的所有标签
	 * @param appId
	 * @param key
	 * @param tagValues
	 * @return
	 */
	boolean removeTags(String appId, String key, String... tagValues);
	
	/**
	 * 删除指定应用ID的所有标签
	 * @param appId
	 * @return
	 */
	boolean removeAllTags(String appId);
	
	/**
	 * 删除所有应用的标签键的标签值。如果标签值(即tagValues)为空，则删除该标签键的所有值，如果标签键和值都为空则删除该应用的所有标签
	 * @param key
	 * @param tagValues
	 * @return
	 */
	boolean removeTagsForAllApps(String key, String... tagValues);
}
