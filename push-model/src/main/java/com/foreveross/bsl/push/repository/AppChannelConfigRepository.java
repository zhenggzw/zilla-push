package com.foreveross.bsl.push.repository;

import com.foreveross.bsl.mongodb.domain.repository.EntityRepository;
import com.foreveross.bsl.push.domain.entity.channel.AppChannelConfig;

public interface AppChannelConfigRepository extends EntityRepository<AppChannelConfig, String>{
	
}
