package com.foreveross.bsl.push.domain.entity;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.foreveross.bsl.mongodb.domain.Entity;

/**
 * 设备签到实体，deviceId+appId可唯一标识实体，
 * 在mongoDB中使用 db.DeviceCheckin.ensureIndex({deviceId: 1, appId: 1}, {unique: true}) 建立唯一索引
 * 标签设置时，如果标签的value包含逗号，则以逗号为分隔符视为多个标签来处理。
 * TODO 暂未考虑tags访问的线程安全问题
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-8
 *
 */
@Document
public class DeviceCheckin extends Entity {

	@Id
	private String id;
	
	@Field("device_id")
	private String deviceId;

	@Field("app_id")
	private String appId;
	
	@Field("token")
	private String pushToken;

	@Field("channel_id")
	private String channelId;

	@Field("device_name")
	private String deviceName;

	@Field("os_name")
	private String osName;

	@Field("os_ver")
	private String osVersion;

	@Field("create_time")
	private Date createTime;

	@Field("checkin_time")
	private Date checkinTime;

	@Field("checkout_time")
	private Date checkoutTime;

	@Field("gis")
	private String gis;

	@Field("alias")
	private String alias;
	
	@Field("online")
	private boolean online;

	/**
	 * 标签集合，在mongoDB中建立索引以增强搜索性能：db.deviceCheckin.ensureIndex({'tags.k':1},{'tags.v':1})
	 */
	@Field("tags")
	private Set<TagEntry> tags;
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getCheckinTime() {
		return checkinTime;
	}

	public void setCheckinTime(Date checkinTime) {
		this.checkinTime = checkinTime;
	}

	public String getGis() {
		return gis;
	}

	public void setGis(String gis) {
		this.gis = gis;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCheckoutTime() {
		return checkoutTime;
	}

	public void setCheckoutTime(Date checkoutTime) {
		this.checkoutTime = checkoutTime;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	/**
	 * @return the tags
	 */
	public Set<TagEntry> getTags() {
		return tags;
	}
	
	public TagsHelper getTagsHelper(){
		return new TagsHelper(this.tags);
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Set<TagEntry> tags) {
		this.tags = tags;
	}

}
