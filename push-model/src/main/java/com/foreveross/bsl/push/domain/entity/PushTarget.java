package com.foreveross.bsl.push.domain.entity;

/**
 * 推送目标
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-10
 *
 */
public class PushTarget {
	
	private String deviceId;
	private String appId;
	private String token;
	private String alias;
	
	public PushTarget() {
	}
	
	public PushTarget(String deviceId, String appId, String token, String alias) {
		super();
		this.deviceId = deviceId;
		this.appId = appId;
		this.token = token;
		this.alias = alias;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
