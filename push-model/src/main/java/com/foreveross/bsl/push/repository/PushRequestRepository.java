package com.foreveross.bsl.push.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.mongodb.domain.repository.EntityRepository;
import com.foreveross.bsl.push.domain.ReceiverTypeEnum;
import com.foreveross.bsl.push.domain.entity.PushRequest;

public interface PushRequestRepository extends EntityRepository<PushRequest, String>{
	Page<PushRequest> findByReceiverTypeAndReceiverValue(ReceiverTypeEnum type, String value, Pageable pageable);
}
