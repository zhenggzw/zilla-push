package com.foreveross.bsl.push.domain.entity.channel;

public class InstantConfig {

    private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
