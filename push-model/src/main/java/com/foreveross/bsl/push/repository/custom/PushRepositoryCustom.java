package com.foreveross.bsl.push.repository.custom;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.push.domain.entity.Push;

public interface PushRepositoryCustom {
	
	void findAndUpdateToSendingByIds(Date sendingTime, Collection<String> pushIds);

	void findAndUpdateToSentByIds(Date sentTime, Collection<String> pushIds);

	void findAndUpdateToOfflineByIds(Collection<String> pushIds, String errorMsg);

	void findAndUpdateToFailureByIds(Collection<String> pushIds, String errorMsg);
	
	Push findOne(String appId, String deviceId, String pushRequestId);
	
	/**
	 * 查找还在有效期的的离线推送
	 * @param appId
	 * @param deviceId
	 * @return
	 */
	Push findOfflinePush(String appId, String deviceId);
	
	/**
	 * 查找已发送但还未回执的还在有效期内的(expired_time<=now)的推送记录
	 * @param deviceId
	 * @param appId
	 * @param pageable
	 * @return
	 */
	Page<Push> findValidSentAndNoReceiptPushs(String deviceId, String appId, Pageable pageable);
	
	List<Push> findExpiredLivePushs();
	
}
