package com.foreveross.bsl.push.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.mongodb.domain.repository.EntityRepository;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.repository.custom.PushRepositoryCustom;

public interface PushRepository extends EntityRepository<Push, String>, PushRepositoryCustom{
	Page<Push> findByTarget_AppId(String appId, Pageable pageable);
	
	Push findByTarget_DeviceIdAndSendId(String deviceId, String sendId);
	
	List<Push> findByTarget_TokenAndTarget_DeviceIdAndStatus(String token, String deviceId, Push.PushStatusEnum status);
}
