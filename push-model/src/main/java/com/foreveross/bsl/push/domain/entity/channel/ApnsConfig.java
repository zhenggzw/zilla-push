package com.foreveross.bsl.push.domain.entity.channel;

import java.util.Date;

public class ApnsConfig {

	private String certFilename;
	private byte[] certContent;
	private String certPassword;
	private Date updateTime;

	public String getCertFilename() {
		return certFilename;
	}

	public void setCertFilename(String certFilename) {
		this.certFilename = certFilename;
	}

	public String getCertPassword() {
		return certPassword;
	}

	public void setCertPassword(String certPassword) {
		this.certPassword = certPassword;
	}

	public byte[] getCertContent() {
		return certContent;
	}

	public void setCertContent(byte[] certContent) {
		if(certContent!=null){
			this.certContent=new byte[certContent.length];
			System.arraycopy(certContent, 0, this.certContent, 0, certContent.length);
		}
		else{
			this.certContent=null;
		}
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
