package com.foreveross.bsl.push.domain.entity;

import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.foreveross.bsl.mongodb.domain.Entity;
import com.foreveross.bsl.push.domain.Message;
import com.foreveross.bsl.push.domain.ReceiverTypeEnum;

@Document
public class PushRequest extends Entity{
	
	@Id
	private String id;
	
	@Field("receiver_type")
	private ReceiverTypeEnum receiverType;
	
	@Field("receiver_value")
	private String receiverValue;
	
	@Field("app_id")
	private String appId;
	
	@Field("tags")
	private Map<String, String> tags;
	
	/**
	 * 保存离线的时长。秒为单位。最多支持10天（864000秒）。
	 * 0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
	 * 此参数不设置则表示默认，默认为保存1天的离线消息（86400秒）。	
	 */
	private long timeToLive=86400; 
	
	@Field("msg")
	private Message message;
	
	@Field("affected_dv")
	private int affectedDevices;
	
	@Field("submit_time")
	private Date submitTime;
	
	@Field("submit_userid")
	private String submitUserId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ReceiverTypeEnum getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(ReceiverTypeEnum receiverType) {
		this.receiverType = receiverType;
	}

	public String getReceiverValue() {
		return receiverValue;
	}

	public void setReceiverValue(String receiverValue) {
		this.receiverValue = receiverValue;
	}

	public int getAffectedDevices() {
		return affectedDevices;
	}

	public void setAffectedDevices(int affectedDevices) {
		this.affectedDevices = affectedDevices;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public long getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * @return the tags
	 */
	public Map<String, String> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

}
