package com.foreveross.bsl.push.repository;

import com.foreveross.bsl.mongodb.domain.repository.EntityRepository;
import com.foreveross.bsl.push.domain.entity.DeviceCheckin;
import com.foreveross.bsl.push.repository.custom.DeviceCheckinRepositoryCustom;

public interface DeviceCheckinRepository extends EntityRepository<DeviceCheckin, String>, DeviceCheckinRepositoryCustom{
	DeviceCheckin findByDeviceIdAndAppId(String deviceId, String appId);
}
