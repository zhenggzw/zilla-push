/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.repository;

import com.foreveross.bsl.mongodb.domain.repository.EntityRepository;
import com.foreveross.bsl.push.domain.entity.TagSummary;
import com.foreveross.bsl.push.repository.custom.TagSummaryRepositoryCustom;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-15
 *
 */
public interface TagSummaryRepository extends EntityRepository<TagSummary, String>,TagSummaryRepositoryCustom {

}
