/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.domain;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.IOSExtra;
import com.foreveross.bsl.push.domain.Message;
import com.foreveross.bsl.push.domain.ReceiverTypeEnum;
import com.foreveross.bsl.push.domain.entity.PushRequest;
import com.foreveross.bsl.push.repository.PushRequestRepository;

/**
 * @description
 *
 * @author Wangyi
 * @version v1.0
 *
 */
public class PushReqestEntityTest extends BaseTestWithSpringIoc{

	@Autowired
	private PushRequestRepository pushReqRepo;
	
	@Test
	public void testCRUD() {
		PushRequest pr=new PushRequest();		
		//msg.setId("1");
		pr.setReceiverType(ReceiverTypeEnum.ALIAS);
		pr.setReceiverValue("wangyi");
		Message msg=new Message();
		msg.setTitle("title");
		msg.setContent("Hello world");
		msg.getExtras().put("ios", new IOSExtra(10,"windows.wav"));
		msg.getExtras().put("city", "gz");
		pr.setMessage(msg);
		pushReqRepo.save(pr);
//		pr.save();
		
		String id=pr.getId();
		Assert.assertNotNull(id);
		
		pr=pushReqRepo.findOne(id);
		Assert.assertNotNull(pr);
		Assert.assertEquals("wangyi", pr.getReceiverValue());
		Assert.assertNotNull(pr.getMessage());
		Assert.assertEquals("gz", pr.getMessage().getExtras().get("city"));
		
		pr.setReceiverValue("java");
		pushReqRepo.save(pr);
		
		pr=pushReqRepo.findOne(id);
		Assert.assertNotNull(pr);
		Assert.assertEquals("java", pr.getReceiverValue());
		
		//XXX BUG: no use to delete object!
//		msg.delete();
		pushReqRepo.delete(id);
		pr=pushReqRepo.findOne(id);
		Assert.assertNull(pr);
	}

}
