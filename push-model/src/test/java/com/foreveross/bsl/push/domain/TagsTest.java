/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

import com.foreveross.bsl.push.domain.entity.TagEntry;
import com.foreveross.bsl.push.domain.entity.TagsHelper;
import com.google.common.collect.Lists;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-2
 *
 */
public class TagsTest {

	@Test
	public void testSetTag(){
		TagsHelper tags=new TagsHelper();
		tags.addTag("role", "机长,管理员");
		Assert.assertEquals(2, tags.getTags().size());
		tags.addTag("city", "广州");
		Assert.assertEquals(3, tags.getTags().size());
	}

	/**
	 * Test method for {@link com.foreveross.bsl.push.domain.entity.TagsHelper#getTagsUseCommaString(java.lang.String)}.
	 */
	@Test
	public void testGetTagsUseCommaString() {
		TagsHelper tags=new TagsHelper();
		tags.addTag("role", "机长,管理员");
		tags.addTag("city", "广州");
		String tagStr=tags.getTagsUseCommaString("role");
		System.out.println(tagStr);
		try {
			Assert.assertEquals("机长,管理员", tagStr);
		} catch (Exception e) {
			Assert.assertEquals("管理员,机长", tagStr);
		}
	}
	
	/**
	 * Test method for {@link com.foreveross.bsl.push.domain.entity.TagsHelper#removeTag(java.lang.String)}.
	 */
	@Test
	public void testRemoveTag() {
		TagsHelper tags=new TagsHelper();
		tags.addTag("role", "机长,管理员");
		tags.addTag("city", "广州");
		tags.removeTag("city");
		Assert.assertEquals(0, tags.getTags("city").length);
		tags.removeTag("role");
		Assert.assertEquals(0, tags.getTags("role").length);
	}

	/**
	 * Test method for {@link com.foreveross.bsl.push.domain.entity.TagsHelper#clearTags()}.
	 */
	@Test
	public void testClearTags() {
		TagsHelper tags=new TagsHelper();
		tags.addTag("role", "机长,管理员");
		tags.addTag("city", "广州");
		tags.clearTags();
		Assert.assertEquals(0, tags.getTags().size());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testUpdateTags(){
		TagsHelper tags=new TagsHelper();
		tags.addTag("role", "机长,管理员");
		tags.addTag("city", "广州");
		Assert.assertEquals(3, tags.getTagsCount());
		tags.updateTag("city", null);
		Assert.assertEquals(2, tags.getTagsCount());
		tags.updateTag("role", "");
		Assert.assertEquals(0, tags.getTagsCount());
		
		tags.addTag("role", "机长,管理员");
		tags.addTag("role", "test");
		//覆盖更新
		tags.updateTag("role", "abc");
		Assert.assertEquals(1, tags.getTagsCount());
		tags.updateTag("city", "gz");
		Assert.assertEquals(2, tags.getTagsCount());
		tags.updateTag("city", "gz");
		Assert.assertEquals(2, tags.getTagsCount());
		tags.removeTag("city");
		Assert.assertEquals(1, tags.getTagsCount());
		
		//清除role标签，再更新新的标签值
		List<TagEntry> tagList=Lists.newArrayList();
		tagList.add(new TagEntry("role",null));
		tagList.add(new TagEntry("role","a,b"));
		tags.updateTags(tagList);
		Assert.assertEquals(2, tags.getTagsCount());
		
		tags.clearTags();
		Assert.assertEquals(0, tags.getTagsCount());
		tags.addTag("os", "Android,ios");
		Assert.assertEquals(2, tags.getTagsCount());
		tags.updateTag("privileges", "机长,com.csair.impc");
		tags.updateTag("os", "fuck,aaa");
		Assert.assertEquals(4, tags.getTagsCount());
		
		Map mixMap=new HashMap();
		mixMap.put("role", "admin");
		mixMap.put("age", 29);
		mixMap.put("birthday", new Date());
		mixMap.put(new Date(), 88888.88f);
		tags.clearTags();
		tags.updateTags(mixMap);
		Assert.assertEquals(4, tags.getTagsCount());
	}
	
}
