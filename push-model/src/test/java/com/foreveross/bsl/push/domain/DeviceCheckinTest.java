/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.domain;

import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;

import com.foreveross.bsl.push.domain.entity.DeviceCheckin;
import com.foreveross.bsl.push.domain.entity.TagEntry;
import com.google.common.collect.Sets;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-15
 *
 */
public class DeviceCheckinTest {

	/**
	 * Test method for {@link com.foreveross.bsl.push.domain.entity.DeviceCheckin#setTags(java.util.Collection)}.
	 */
	@Test
	public void testSetTags() {
		DeviceCheckin dc=new DeviceCheckin();
		Set<TagEntry> tags=Sets.newHashSet();
		tags.add(new TagEntry("role","机长,管理员"));
		tags.add(new TagEntry("city","广州"));
		dc.setTags(tags);
		Assert.assertEquals(3, dc.getTags().size());
	}
	
}
