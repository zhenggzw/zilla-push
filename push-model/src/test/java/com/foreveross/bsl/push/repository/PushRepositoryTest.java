/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.Push.PushStatusEnum;
import com.foreveross.bsl.push.domain.entity.PushTarget;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-10-12
 * 
 */
public class PushRepositoryTest extends BaseTestWithSpringIoc {
	
	private static final Logger log = LoggerFactory.getLogger(PushRepositoryTest.class);

	@Autowired
	private PushRepository pushRepo;

	private String token = "1", deviceId = "1", appId = "1";

	@Test
	public void testFindValidSentAndNoReceiptPushs() throws InterruptedException {
		List<Push> pushs=new ArrayList<Push>();
		try {
			Pageable pageable = new PageRequest(0, 10);
			long timeToLive = 1000 * 5;
			pushs.add(addPush("1", timeToLive));
			
			Page<Push> page = pushRepo.findValidSentAndNoReceiptPushs(deviceId, appId, pageable);
			Assert.assertEquals(1, page.getTotalElements());
			pushs.add(addPush("2", timeToLive));
			page = pushRepo.findValidSentAndNoReceiptPushs(deviceId, appId, pageable);
			Assert.assertEquals(2, page.getTotalElements());
			
			Thread.sleep(timeToLive);
			page = pushRepo.findValidSentAndNoReceiptPushs(deviceId, appId, pageable);
			Assert.assertEquals(0, page.getTotalElements());
			
			pushs.add(addPush("3", timeToLive));
			page = pushRepo.findValidSentAndNoReceiptPushs(deviceId, appId, pageable);
			Assert.assertEquals(1, page.getTotalElements());
			
		} finally {
			this.pushRepo.delete(pushs);
		}
	}

	private Push addPush(String id, long timeToLive) {
		Date now = new Date();
		Push p = new Push();
		p.setId(id);
		p.setChannelId("apns");
		p.setExpiredTime(new Date(now.getTime() + timeToLive));
		p.setCreateTime(now);
		p.setStatus(PushStatusEnum.SENT);
		p.setTimeToLive(timeToLive);
		p.setTarget(new PushTarget(deviceId, appId, token, null));
		pushRepo.save(p);
		return p;
	}

}
