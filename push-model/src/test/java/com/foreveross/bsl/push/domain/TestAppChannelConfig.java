package com.foreveross.bsl.push.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.entity.channel.ApnsConfig;
import com.foreveross.bsl.push.domain.entity.channel.AppChannelConfig;
import com.foreveross.bsl.push.domain.entity.channel.OpenfireConfig;
import com.foreveross.bsl.push.repository.AppChannelConfigRepository;
import com.google.common.io.Files;

public class TestAppChannelConfig extends BaseTestWithSpringIoc {

	@Autowired
	private AppChannelConfigRepository cfgRepo;
	private String appId = "51f5e10f07ef740d468a6370";

	@After
	public void tearDown() {
//		this.cfgRepo.delete(appId);
//		AppChannelConfig cfg = this.cfgRepo.findOne(appId);
//		assertNull(cfg);
	}

	@Test
	public void testCRUD() throws IOException {
		final File dataFile = new File(System.getProperty("user.dir") + "/src/test/resources/chameleon.p12");
		final byte[] content = Files.toByteArray(dataFile);
		AppChannelConfig cfg = new AppChannelConfig();
		cfg.setAppId(appId);
		cfg.setCreateTime(new Date());
		ApnsConfig apns = new ApnsConfig();
		apns.setCertContent(content);
		apns.setCertFilename("chameleon.p12");
		apns.setCertPassword("123456");
		cfg.setApnsConfig(apns);
		OpenfireConfig of = new OpenfireConfig();
		of.setUsername("test");
		of.setPassword("ppp");
		cfg.setOpenfireConfig(of);
		this.cfgRepo.save(cfg);

		cfg = this.cfgRepo.findOne(cfg.getAppId());
		assertNotNull(cfg);
		assertEquals("chameleon.p12", cfg.getApnsConfig().getCertFilename());
		assertEquals("123456", cfg.getApnsConfig().getCertPassword());
		assertTrue(Arrays.equals(content, cfg.getApnsConfig().getCertContent()));

		Page<AppChannelConfig> page = cfgRepo.findByQuery(Query.query(Criteria.where("appId").is(appId)).limit(
				10));
		Assert.assertEquals(1, page.getContent().size());
		
	}

}
