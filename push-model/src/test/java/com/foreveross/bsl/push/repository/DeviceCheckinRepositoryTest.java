package com.foreveross.bsl.push.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.foreveross.bsl.common.utils.mapper.BeanMapper;
import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.entity.DeviceCheckin;
import com.foreveross.bsl.push.domain.entity.TagsHelper;
import com.google.common.collect.Maps;

public class DeviceCheckinRepositoryTest extends BaseTestWithSpringIoc {

	@Autowired
	private DeviceCheckinRepository dcRepo;

	@Test
	public void testCRUD() {
		DeviceCheckin dc = new DeviceCheckin();
		dc.setAlias("wangyi");
		dc.setAppId("bslxxx");
		dc.setCreateTime(new Date());
		dc.setCheckinTime(new Date());
		dc.setDeviceId("0000-ffff0-8888");
		dc.setDeviceName("Lenove");
		dc.setGis("123.234,225,56");
		dc.setOsName("Android");
		dc.setOsVersion("v4.3");
		dc.setPushToken("af235435");
		TagsHelper tags = new TagsHelper();
		tags.addTag("level", "gold").addTag("location", "gz").addTag("lang", "java");
		dc.setTags(tags.getTags());
		dcRepo.save(dc);

		String id = dc.getId();
		try {
			dc = dcRepo.findOne(id);
			Assert.assertNotNull(dc);
			Assert.assertEquals("wangyi", dc.getAlias());
			Assert.assertNotNull(dc.getCheckinTime());
			Assert.assertNotNull(dc.getTags());
			Assert.assertEquals(3, dc.getTags().size());
			Assert.assertEquals("gold", dc.getTagsHelper().getTag("level"));

			dc.setDeviceName("GT9001");
			tags.clearTags();
			tags.addTag("level", "sliver").addTag("lang", "c++").removeTag("location");
			dc.setTags(tags.getTags());
			dcRepo.save(dc);
			dc = dcRepo.findOne(id);
			Assert.assertNotNull(dc);
			Assert.assertEquals("GT9001", dc.getDeviceName());
			Assert.assertNotNull(dc.getTags());
			Assert.assertEquals(2, dc.getTags().size());
			Assert.assertEquals("sliver", dc.getTagsHelper().getTag("level"));
		} finally {
			dcRepo.delete(id);
			dc = dcRepo.findOne(id);
			Assert.assertNull(dc);
		}
	}

	@Test
	public void testFindByTags() {
		String appId = "bsl_abc";
		DeviceCheckin dc = new DeviceCheckin();
		dc.setAlias("wangyi");
		dc.setAppId(appId);
		dc.setCreateTime(new Date());
		dc.setCheckinTime(new Date());
		dc.setDeviceId("0000-ffff0-8888");
		dc.setDeviceName("Lenove");
		dc.setGis("123.23,225.58");
		dc.setOsName("Android");
		dc.setOsVersion("v4.3");
		dc.setPushToken("af235435");
		TagsHelper tags = new TagsHelper();
		tags.addTag("level", "gold").addTag("location", "gz").addTag("lang", "java").addTag("role", "机长,管理员");
		dc.setTags(tags.getTags());
		dcRepo.save(dc);

		DeviceCheckin dc1 = BeanMapper.map(dc, DeviceCheckin.class);
		dc1.setId(null);
		dc1.setDeviceId("0000-ffff0-8899");
		dc1.setAlias("tracy");
		tags.clearTags();
		tags.addTag("level", "gold").addTag("location", "sz").removeTag("lang").addTag("role", "机长");
		dc1.setTags(tags.getTags());
		dcRepo.save(dc1);

		DeviceCheckin dc2 = BeanMapper.map(dc1, DeviceCheckin.class);
		dc2.setId(null);
		dc2.setDeviceId("0000-ffff0-7899");
		dc2.setAlias("pp");
		tags.clearTags();
		tags.addTag("level", "sliver").addTag("location", "sz").addTag("born", "gz").addTag("role", "管理员");
		dc2.setTags(tags.getTags());
		dcRepo.save(dc2);

		try {
			Map<String, String> map = Maps.newHashMap();
			map.put("level", "gold,sliver");
			// find by level is gold OR sliver
			List<DeviceCheckin> list = dcRepo.findByTags(map, appId);
			Assert.assertNotNull(list);
			Assert.assertEquals(3, list.size());

			map.put("location", "gz");
			// find by level is gold OR sliver AND location is gz
			list = null;
			list = dcRepo.findByTags(map, appId);
			Assert.assertNotNull(list);
			Assert.assertEquals(1, list.size());

			map.clear();
			map.put("role", "机长,管理员");
			// find by role is 机长 OR 管理员
			list = dcRepo.findByTags(map, appId);
			Assert.assertNotNull(list);
			Assert.assertEquals(3, list.size());

			map.clear();
			map.put("role", "机长");
			// find by role is 机长
			list = dcRepo.findByTags(map, appId);
			Assert.assertNotNull(list);
			Assert.assertEquals(2, list.size());

			map.clear();
			map.put("role", "管理员");
			// find by role is 管理员
			list = dcRepo.findByTags(map, appId);
			Assert.assertNotNull(list);
			Assert.assertEquals(2, list.size());
		} finally {
			dcRepo.delete(dc.getId());
			dcRepo.delete(dc1.getId());
			dcRepo.delete(dc2.getId());
		}
	}
	
	@Test
	public void testClearToken(){
		DeviceCheckin dc = new DeviceCheckin();
		dc.setAlias("wangyi");
		dc.setAppId("bslxxx");
		dc.setCreateTime(new Date());
		dc.setCheckinTime(new Date());
		dc.setDeviceId("0000-ffff0-8888");
		dc.setDeviceName("Lenove");
		dc.setGis("123.234,225,56");
		dc.setOsName("Android");
		dc.setOsVersion("v4.3");
		dc.setPushToken("test-235435000909092432");
		try{
			dcRepo.save(dc);
			int n=dcRepo.clearToken(dc.getPushToken());
			Assert.assertEquals(1, n);
			dc=dcRepo.findOne(dc.getId());
			Assert.assertNull(dc.getPushToken());
		} finally{
			dcRepo.delete(dc);
		}
	}
}
