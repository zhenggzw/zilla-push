/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel;

import java.util.Date;

import com.foreveross.bsl.push.application.impl.DsRouterPushId;

/**
 * 渠道发送反馈接口
 * TODO pushId应该改成push对象，上线后再重构
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-9-3
 * 
 */
public interface SendFeedbackable {
	
	/**
	 * 发送成功
	 * 
	 * @param push
	 * @param sentTime
	 */
	void sentSuccessfully(Date sentTime, DsRouterPushId... pushIds);

	/**
	 * 发送失败，特指由网络通信相关导致的异常,如手机终端不在线,渠道无法发送等
	 * 
	 * @param push
	 * @param msg
	 * @param e
	 */
	void communicationFailure(String msg, Throwable e, DsRouterPushId... pushIds);

	/**
	 * 不可恢复的异常，一般包括消息格式、长度、token等错误导致的异常
	 * @param push
	 * @param msg
	 * @param e
	 */
	void unrecoverableFailure(String msg, Throwable e, DsRouterPushId... pushIds);
	
	/**
	 * 能重发的异常
	 * @param push
	 * @param msg
	 * @param e
	 */
	void couldRetryFailure(String msg, Throwable e, DsRouterPushId... pushIds);
}
