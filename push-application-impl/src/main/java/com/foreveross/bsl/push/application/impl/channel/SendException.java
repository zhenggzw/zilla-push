package com.foreveross.bsl.push.application.impl.channel;

import com.foreveross.bsl.push.application.PushModuleException;

public class SendException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6611238860233719934L;

	public SendException() {
	}

	public SendException(String msg) {
		super(msg);
	}

	public SendException(Throwable clause) {
		super(clause);
	}

	public SendException(String msg, Throwable clause) {
		super(msg, clause);
	}

}
