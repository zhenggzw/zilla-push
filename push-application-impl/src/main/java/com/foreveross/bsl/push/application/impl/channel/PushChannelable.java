package com.foreveross.bsl.push.application.impl.channel;

import java.util.Set;

import com.foreveross.bsl.push.domain.Message;

public interface PushChannelable {
	
	String getChannelId();

	SendReport send(Set<SendTarget> targets, Message msg);

	void close();

	boolean addSendListener(SendListener sendListener);

	boolean removeSendListener(SendListener sendListener);
}
