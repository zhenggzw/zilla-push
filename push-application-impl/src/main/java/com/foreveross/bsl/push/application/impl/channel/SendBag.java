/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.foreveross.bsl.push.domain.entity.PushRequest;
import com.google.common.base.Stopwatch;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-11-8
 *
 */
public class SendBag {

	private final static Logger log = LoggerFactory.getLogger(SendBag.class);

	private final PushChannelable channel;
	private final PushRequest pushRequest;
	private final Set<SendTarget> targets;
	private final String bagNo;

	public SendBag(PushChannelable channel, PushRequest pushRequest, Set<SendTarget> targets, String bagNo) {
		super();
		Assert.notNull(channel, "channel must not be null");
		Assert.notNull(pushRequest, "pushRequest must not be null");
		Assert.notEmpty(targets, "targets must not be empty");
		Assert.hasText(bagNo, "bagNo must not be empty");

		this.channel = channel;
		this.pushRequest = pushRequest;
		this.targets = targets;
		this.bagNo = bagNo;
	}

	public void execute() {
		log.debug("使用推送渠道:{}处理推送", channel.getChannelId());

		Stopwatch stopwatch = new Stopwatch();
		stopwatch.start();
		this.channel.send(targets, pushRequest.getMessage()); // 发送消息
		log.trace("channel.send() cost {}ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));

		stopwatch.stop();
	}

	/**
	 * @return the channel
	 */
	public PushChannelable getChannel() {
		return channel;
	}

	/**
	 * @return the targets
	 */
	public Set<SendTarget> getTargets() {
		return targets;
	}

	/**
	 * @return the pushRequest
	 */
	public PushRequest getPushRequest() {
		return pushRequest;
	}

	/**
	 * @return the bagNo
	 */
	public String getBagNo() {
		return bagNo;
	}

}
