/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import java.util.List;

import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.common.utils.mapper.BeanMapper;
import com.foreveross.bsl.push.application.PushMessageQueryService;
import com.foreveross.bsl.push.application.PushModuleException;
import com.foreveross.bsl.push.application.vo.MessageVo;
import com.foreveross.bsl.push.domain.Message;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.PushRequest;
import com.foreveross.bsl.push.repository.PushRepository;
import com.foreveross.bsl.push.repository.PushRequestRepository;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-11-8
 *
 */
@Named
public class DefaultPushMessageQueryService implements PushMessageQueryService {

	private final PushRepository getPushRepository(){
		return new Push().getEntityRepository(PushRepository.class);
	}

	private final PushRequestRepository getPushRequestRepository(){
		return new PushRequest().getEntityRepository(PushRequestRepository.class);
	}
	
	/* (non-Javadoc)
	 * @see com.foreveross.bsl.push.application.PushMessageQueryService#get(java.lang.String)
	 */
	@Override
	public MessageVo get(String appId, String id) {
		PushRequest pr=getPushRequestRepository().findOne(id);
		if(pr==null){
			throw new PushModuleException("不存在消息推送请求:"+id);
		}
		return this.convertToVo(pr.getMessage());
	}
	
	private MessageVo convertToVo(Message msg){
		MessageVo vo=BeanMapper.map(msg, MessageVo.class);
		//Dozer在extras map中有custom obj list时映射会有问题
		vo.setExtras(Maps.newHashMap(msg.getExtras()));
		return vo;
	}
	
	@Override
	public List<MessageVo> fetchSentAndNoReceiptMessages(String deviceId, String appId, Integer pageNumber, Integer pageSize) {
		Page<MessageVo> page=this.fetchSentAndNoReceiptMessagePage(deviceId, appId, pageNumber, pageSize);
		return page.getContent();
	}
	
	/* (non-Javadoc)
	 * @see com.foreveross.bsl.push.application.PushMessageQueryService#fetchSentAndNoReceiptMessagePage(java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Page<MessageVo> fetchSentAndNoReceiptMessagePage(String deviceId, String appId, Integer pageNumber, Integer pageSize) {
		if(StringUtils.isEmpty(deviceId)){
			throw new IllegalArgumentException("deviceId can not be empty");
		}
		if(StringUtils.isEmpty(appId)){
			throw new IllegalArgumentException("appId can not be empty");
		}
		
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);
		Page<Push> pushs = this.getPushRepository().findValidSentAndNoReceiptPushs(deviceId, appId, pageable);
		
		List<MessageVo> msgs=Lists.newArrayListWithCapacity(pushs.getContent().size());
		for(Push p : pushs){
			PushRequest pr=p.getRequest();
			if(pr==null){
				//可能脏数据导致为null
				continue;
			}
			msgs.add(convertToVo(pr.getMessage()));
		}
		return new PageImpl<MessageVo>(msgs, pageable, pushs.getTotalElements());
	}

	/* (non-Javadoc)
	 * @see com.foreveross.bsl.push.application.PushMessageQueryService#fetchSentAndNoReceiptMessages1(java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<MessageVo> fetchSentAndNoReceiptMessages1(String deviceId, String appId, Integer pageNumber, Integer pageSize) {
		return this.fetchSentAndNoReceiptMessages(deviceId, appId, pageNumber, pageSize);
	}

}
