package com.foreveross.bsl.push.application.impl.channel;

public interface SendListener {
	void sending(SendingEvent event);
	void sent(SentEvent event);
}
