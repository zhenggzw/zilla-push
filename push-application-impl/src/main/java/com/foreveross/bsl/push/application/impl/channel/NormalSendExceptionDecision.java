/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel;

import java.net.SocketTimeoutException;

import com.foreveross.bsl.push.application.impl.DsRouterPushId;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-4
 *
 */
public class NormalSendExceptionDecision {

	private SendFeedbackable feedbacker;
	
	public NormalSendExceptionDecision(SendFeedbackable feedbacker){
		this.feedbacker=feedbacker;
	}
	
	public void handleException(Throwable e, DsRouterPushId... pushIds){
		if(e instanceof SocketTimeoutException){
			//feedbacker.couldRetryFailure(e.getMessage(), e, pushIds);
			feedbacker.communicationFailure(e.getMessage(), e, pushIds);
		}
		else{
			feedbacker.unrecoverableFailure(e.getMessage(), e, pushIds);
		}
	}
}
