package com.foreveross.bsl.push.application.impl.channel;

import java.util.Collection;

import com.google.common.collect.Lists;

public class SendReport {
	private boolean perfect;
	private final Collection<FailEntry> failEntrys=Lists.newArrayList();
	
	public SendReport() {
		this.perfect=true;
	}
	
	public class FailEntry{
		private final SendTarget sendTarget;
		private final Exception exception;
		
		public FailEntry(SendTarget sendTarget, Exception exception) {
			super();
			this.sendTarget = sendTarget;
			this.exception = exception;
		}

		public SendTarget getSendTarget() {
			return sendTarget;
		}

		public Exception getException() {
			return exception;
		}
	}

	public boolean isPerfect() {
		return perfect;
	}
	
	public Collection<FailEntry> getFailEntrys() {
		return failEntrys;
	}
	
	public SendReport fail(SendTarget sendTarget, Exception exception){
		if(this.perfect){
			this.perfect=false;
		}
		this.failEntrys.add(new FailEntry(sendTarget, exception));
		return this;
	}
	
}

