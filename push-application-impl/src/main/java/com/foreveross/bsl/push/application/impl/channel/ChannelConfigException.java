/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel;

import com.foreveross.bsl.push.application.PushModuleException;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-30
 *
 */
public class ChannelConfigException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2561640184518838471L;

	/**
	 * @param message
	 * @param cause
	 */
	public ChannelConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ChannelConfigException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ChannelConfigException(Throwable cause) {
		super(cause);
	}

}
