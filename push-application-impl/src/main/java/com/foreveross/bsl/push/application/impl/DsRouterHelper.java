/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import com.foreveross.bsl.common.utils.context.ContextThead;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-4
 *
 */
public class DsRouterHelper {
	
	private DsRouterHelper() {
	}

	public static String getCurrentDsRouter(){
		return ContextThead.getThreadVar();
	}
	
	public static void setCurrentDsRouter(String dsRouter){
		ContextThead.setThreadVar(dsRouter);
	}
}
