package com.foreveross.bsl.push.application.impl;

import javax.inject.Named;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.common.utils.enums.OperatEnum;
import com.foreveross.bsl.common.utils.mapper.BeanMapper;
import com.foreveross.bsl.common.utils.web.QueryFilter;
import com.foreveross.bsl.common.utils.web.QueryOrder;
import com.foreveross.bsl.common.utils.web.QuerySpecs;
import com.foreveross.bsl.push.application.PushMgmtService;
import com.foreveross.bsl.push.application.vo.PushRequestVo;
import com.foreveross.bsl.push.application.vo.PushVo;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.PushRequest;
import com.foreveross.bsl.push.repository.PushRepository;
import com.foreveross.bsl.push.repository.PushRequestRepository;

@Named("defaultPushMgmtService")
public class DefaultPushMgmtService implements PushMgmtService {

	private final PushRepository getPushRepository(){
		return new Push().getEntityRepository(PushRepository.class);
	}

	private final PushRequestRepository getPushRequestRepository(){
		return new PushRequest().getEntityRepository(PushRequestRepository.class);
	}

	private Page<PushRequestVo> convertToPushRequstVoPage(Page<PushRequest> page, Pageable pageable) {
		Page<PushRequestVo> result = new PageImpl<PushRequestVo>(BeanMapper.mapList(page.getContent(), PushRequestVo.class),
				pageable, page.getTotalElements());
		return result;
	}

	private Page<PushVo> convertToPushVoPage(Page<Push> page, Pageable pageable) {
		Page<PushVo> result = new PageImpl<PushVo>(BeanMapper.mapList(page.getContent(), PushVo.class), pageable,
				page.getTotalElements());
		return result;
	}
	
	@Override
	public Page<PushRequestVo> findPushRequestsByUser(String userId, int pageNumber, int pageSize) {
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);
		Page<PushRequest> page = getPushRequestRepository().findByProperty("submitUserId", OperatEnum.EQ, userId, pageable);
		return convertToPushRequstVoPage(page, pageable);
	}
	
	/* (non-Javadoc)
	 * @see com.foreveross.bsl.push.application.PushMgmtService#findPushRequestsByApp(java.lang.String, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<PushRequestVo> findPushRequestsByApp(String appId, int pageNumber, int pageSize, QuerySpecs querySpecs) {
		if(querySpecs==null){
			querySpecs=new QuerySpecs();
		}
		QueryOrder qo=querySpecs.getOrder("submit_time");
		if(qo==null){
			querySpecs.setOrder("submit_time", QueryOrder.Direction.DESC);
		}
		querySpecs.addFilter(QueryFilter.eq("app_id", appId));
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);

		Page<PushRequest> page = getPushRequestRepository().findByQuerySpecs(querySpecs, pageable);
		return convertToPushRequstVoPage(page, pageable);
	}

	@Override
	public Page<PushRequestVo> findPushRequests(int pageNumber, int pageSize, QuerySpecs querySpecs) {
		if(querySpecs==null){
			querySpecs=new QuerySpecs();
		}
		QueryOrder qo=querySpecs.getOrder("submit_time");
		if(qo==null){
			querySpecs.setOrder("submit_time", QueryOrder.Direction.DESC);
		}
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);

		Page<PushRequest> page = getPushRequestRepository().findByQuerySpecs(querySpecs, pageable);
		return convertToPushRequstVoPage(page, pageable);
	}

	@Override
	public Page<PushVo> findPushsByRequest(String requestId, int pageNumber, int pageSize, QuerySpecs querySpecs) {
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);
		if(querySpecs==null){
			querySpecs=new QuerySpecs();
		}
		//如果PushRequest的id是mongoDB自动生成ID,则查询时要new ObjectId(requestId)
		querySpecs.addFilter(QueryFilter.eq("request.$id", requestId));
		Page<Push> page = getPushRepository().findByQuerySpecs(querySpecs, pageable);
		return convertToPushVoPage(page, pageable);
	}

	@Override
	public Page<PushVo> findPushsByApp(String appId, int pageNumber, int pageSize, QuerySpecs querySpecs) {
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);
		Page<Push> page = getPushRepository().findByTarget_AppId(appId, pageable);
		return convertToPushVoPage(page, pageable);
	}

	@Override
	public Page<PushVo> findPushs(int pageNumber, int pageSize, QuerySpecs querySpecs) {
		Pageable pageable=PageRequestHelper.buildPageable(pageNumber, pageSize);
		Page<Push> page = this.getPushRepository().findAll(pageable);
		return convertToPushVoPage(page, pageable);
	}

}
