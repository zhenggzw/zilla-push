/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import javax.inject.Named;

import org.springframework.util.Assert;

import com.foreveross.bsl.common.utils.mapper.BeanMapper;
import com.foreveross.bsl.push.application.TagSummaryService;
import com.foreveross.bsl.push.application.vo.TagSummaryVo;
import com.foreveross.bsl.push.domain.entity.TagSummary;
import com.foreveross.bsl.push.domain.entity.TagSummary.TagSet;
import com.foreveross.bsl.push.repository.TagSummaryRepository;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-15
 *
 */
@Named("defaultTagSummaryService")
public class DefaultTagSummaryService implements TagSummaryService {
	
	private final TagSummaryRepository getTagSummaryRepository(){
		return new TagSummary().getEntityRepository(TagSummaryRepository.class);
	}
	
	@Override
	public void updateTagSummary(String appId, String key, String... tagValues) {
		Assert.notNull(appId, "appId can not be null");
		Assert.notNull(key, "key can not be null");
		Assert.notNull(tagValues, "tagValues can not be null");
		this.getTagSummaryRepository().updateTags(appId, key, tagValues);
	}
	
	@Override
	public TagSummaryVo getTagSummary(String appId) {
		Assert.notNull(appId, "appId param can't be null");
		TagSummary ts=getTagSummaryRepository().findOne(appId);
		if(ts!=null){
			return BeanMapper.map(ts, TagSummaryVo.class);
		}
		return null;
	}
	
	@Override
	public String[] getTagValues(String appId, String key) {
		Assert.notNull(appId, "appId param can't be null");
		Assert.notNull(key, "key param can't be null");
		TagSummary ts=getTagSummaryRepository().findOne(appId);
		if(ts!=null){
			TagSet tagSet=ts.getTagSet(key);
			if(tagSet!=null){
				return tagSet.getValues();
			}
		}
		return null;
	}

	@Override
	public void removeTag(String appId, String key, String... values) {
		Assert.notNull(appId, "appId param can't be null");
		Assert.notNull(key, "key param can't be null");
		Assert.notNull(values, "values param can't be null");
		this.getTagSummaryRepository().removeTags(appId, key, values);	
	}

	@Override
	public void removeTag(String appId, String key) {
		Assert.notNull(appId, "appId param can't be null");
		Assert.notNull(key, "key param can't be null");
		this.getTagSummaryRepository().removeTags(appId, key);
	}

	@Override
	public void delete(String appId) {
		Assert.notNull(appId, "appId param can't be null");
		this.getTagSummaryRepository().delete(appId);
	}
	
	@Override
	public void removeAllTags(String appId) {
		Assert.notNull(appId, "appId param can't be null");
		this.getTagSummaryRepository().removeAllTags(appId);
	}

	@Override
	public void removeTagForAllApps(String key, String... values) {
		Assert.notNull(key, "key param can't be null");
		Assert.notNull(values, "values param can't be null");
		this.getTagSummaryRepository().removeTagsForAllApps(key, values);
	}

	@Override
	public void removeTagForAllApps(String key) {
		Assert.notNull(key, "key param can't be null");
		this.getTagSummaryRepository().removeTagsForAllApps(key);
	}
	
}
