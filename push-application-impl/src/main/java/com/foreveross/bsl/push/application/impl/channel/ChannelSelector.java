package com.foreveross.bsl.push.application.impl.channel;

public interface ChannelSelector {
	PushChannelable selectChannel(String channelId) throws ChannelNotFoundException;
}
