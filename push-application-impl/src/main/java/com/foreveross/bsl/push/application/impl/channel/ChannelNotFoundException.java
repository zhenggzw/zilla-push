package com.foreveross.bsl.push.application.impl.channel;

import com.foreveross.bsl.push.application.PushModuleException;

public class ChannelNotFoundException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1776102166020719795L;

	public ChannelNotFoundException() {

	}

	public ChannelNotFoundException(String msg) {
		super(msg);
	}
	
}
