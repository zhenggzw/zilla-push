/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import com.foreveross.bsl.push.application.DsRouterIdentifiable;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-5
 *
 */
public class DsRouterPushId implements DsRouterIdentifiable {
	
	private String pushId;
	private String dsRouter;
	
	public DsRouterPushId(String pushId, String dsRouter) {
		this.pushId = pushId;
		this.dsRouter = dsRouter;
	}

	@Override
	public void setDsRouter(String dsRouter) {
		this.dsRouter = dsRouter;
	}

	@Override
	public String getDsRouter() {
		return dsRouter;
	}
	
	/**
	 * @return the pushId
	 */
	public String getPushId() {
		return pushId;
	}
	/**
	 * @param pushId the pushId to set
	 */
	public void setPushId(String pushId) {
		this.pushId = pushId;
	}
	
	@Override
	public String toString() {
		return "dsRouter:"+dsRouter+", pushId:"+pushId;
	}

}
