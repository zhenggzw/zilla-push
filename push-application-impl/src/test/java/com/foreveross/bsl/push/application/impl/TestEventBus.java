/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import java.util.concurrent.Executors;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-10
 * 
 */
public class TestEventBus {
	
	private static final Logger log = LoggerFactory.getLogger(TestEventBus.class);

	@Test
	public void test() {
		EventBus eventBus = new AsyncEventBus(Executors.newFixedThreadPool(2));
		PurchaseSubscriber purchaseSubscriber = new PurchaseSubscriber();
		eventBus.register(purchaseSubscriber);
		eventBus.register(new CashPurchaseSubscriber());
		eventBus.register(new CreditPurchaseSubscriber());
		log.info("begin post cash purchase...");
		eventBus.post(new CashPurchaseEvent("AAA", 100));
		log.info("begin post creidt purchase...");
		eventBus.post(new CreditPurchaseEvent("AAA", 100, "3245 2432 23299"));
	}

}

abstract class PurchaseEvent {
	String item;

	public PurchaseEvent(String item) {
		this.item = item;
	}
}

class CashPurchaseEvent extends PurchaseEvent {
	int amount;

	public CashPurchaseEvent(String item, int amount) {
		super(item);
		this.amount = amount;
	}
}

class CreditPurchaseEvent extends PurchaseEvent {
	int amount;
	String cardNumber;

	public CreditPurchaseEvent(String item, int amount, String cardNumber) {
		super(item);
		this.amount = amount;
		this.cardNumber = cardNumber;
	}
}

// Would only be notified of Cash purchase events
class CashPurchaseSubscriber {
	private static final Logger log = LoggerFactory.getLogger(CashPurchaseSubscriber.class);
	@Subscribe
	public void handleCashPurchase(CashPurchaseEvent event) {
		log.info("handle cash purchase");
	}
}

// Would only be notified of credit purchases
class CreditPurchaseSubscriber {
	private static final Logger log = LoggerFactory.getLogger(CreditPurchaseSubscriber.class);
	@Subscribe
	public void handleCreditPurchase(CreditPurchaseEvent event) {
		log.info("handle credit purchase");
	}
}

// Notified of any purchase event
class PurchaseSubscriber {
	private static final Logger log = LoggerFactory.getLogger(PurchaseSubscriber.class);
	@Subscribe
	public void handlePurchaseEvent(PurchaseEvent event) {
		log.info("handle purchase");
	}
}