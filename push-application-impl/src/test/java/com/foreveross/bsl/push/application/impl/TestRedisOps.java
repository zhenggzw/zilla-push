package com.foreveross.bsl.push.application.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "/applicationContext-redis-test.xml" })
public class TestRedisOps extends AbstractJUnit4SpringContextTests {

	@Autowired
	private RedisTemplate<String, String> template;
	
	@Test
	public void testOpsForValue(){
		this.template.opsForValue().set("number", "9999999");
		Assert.assertEquals("9999999", this.template.opsForValue().get("number"));
		
		this.template.opsForList().rightPushAll("rlist", "a", "b", "c");
		List<String> list=this.template.opsForList().range("rlist", 0, 2);
		System.out.println(list);
		
	}
}
