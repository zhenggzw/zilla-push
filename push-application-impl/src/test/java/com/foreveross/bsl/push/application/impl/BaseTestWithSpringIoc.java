package com.foreveross.bsl.push.application.impl;

import org.junit.Before;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.foreveross.bsl.common.utils.factory.InstanceFactory;
import com.foreveross.bsl.common.utils.factory.InstanceSpringProvider;

@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class BaseTestWithSpringIoc extends AbstractJUnit4SpringContextTests{

	@Before
	public void before(){
		InstanceSpringProvider provider = new InstanceSpringProvider(this.applicationContext);
		InstanceFactory.setInstanceProvider(provider);
	}
	
}
