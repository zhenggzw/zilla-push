package com.foreveross.bsl.push.application.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.foreveross.bsl.common.utils.mapper.BeanMapper;
import com.foreveross.bsl.push.application.impl.channel.SendBagBuilder;
import com.foreveross.bsl.push.application.impl.channel.SendTarget;
import com.foreveross.bsl.push.application.impl.channel.SendTarget.TargetIdentity;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.Push.PushStatusEnum;
import com.foreveross.bsl.push.domain.entity.PushTarget;
import com.google.common.collect.Lists;

public class TestSendBagBuilder {

	@Test
	public void testBuildChannelSendTargets(){
		List<Push> pushs=buildPushList();
		SendBagBuilder sbb=new SendBagBuilder();;
		Map<String, Set<SendTarget>> channelSends=sbb.buildSendTargetMap("a123", pushs);
		Assert.assertEquals(2, channelSends.size());
		Assert.assertTrue(channelSends.containsKey("openfire"));
		Assert.assertTrue(channelSends.containsKey("apns"));
		
		Set<SendTarget> stsOpenfire=channelSends.get("openfire");
		Assert.assertNotNull(stsOpenfire);
		Assert.assertEquals(2, stsOpenfire.size());
		for(SendTarget st : stsOpenfire){
			Assert.assertTrue(st.getAppId().equals("app1") || st.getAppId().equals("app2"));
			if(st.getAppId().equals("app1")){
				Assert.assertEquals(2, st.getTargets().size());
				Assert.assertTrue(st.getTargets().contains(new TargetIdentity("1", "1", "p1")));
				Assert.assertTrue(st.getTargets().contains(new TargetIdentity("2", "2", "p2")));
			}
			else if(st.getAppId().equals("app2")){
				Assert.assertEquals(2, st.getTargets().size());
				Assert.assertTrue(st.getTargets().contains(new TargetIdentity("3", "3", "p3")));
				Assert.assertTrue(st.getTargets().contains(new TargetIdentity("4", "4", "p4")));
			}
		}
		
		Set<SendTarget> stsApns=channelSends.get("apns");
		Assert.assertNotNull(stsApns);
		Assert.assertEquals(1, stsApns.size());
		SendTarget st=stsApns.iterator().next();
		Assert.assertEquals("app3", st.getAppId());
		Assert.assertEquals(2, st.getTargets().size());
		Assert.assertTrue(st.getTargets().contains(new TargetIdentity("5", "5", "p5")));
		Assert.assertTrue(st.getTargets().contains(new TargetIdentity("6", "6", "p6")));
	}
	
	private List<Push> buildPushList(){
		List<Push> pushs=Lists.newArrayList();
		String deviceId="1", 
				token="1", 
				channelId="openfire", 
				appId="app1";
		Push push=new Push();
		push.setId("p1");
		push.setCreateTime(new Date());
		push.setStatus(PushStatusEnum.PENDING);
		push.setTarget(new PushTarget(deviceId, appId, token, "test"));
		push.setChannelId(channelId);
		pushs.add(push);
		
		push=BeanMapper.map(push, Push.class);
		push.setId("p2");
		push.getTarget().setDeviceId("2");
		push.getTarget().setToken("2");
		pushs.add(push);
		
		push=BeanMapper.map(push, Push.class);
		push.setId("p3");
		push.getTarget().setDeviceId("3");
		push.getTarget().setToken("3");
		push.getTarget().setAppId("app2");
		pushs.add(push);
		
		push=BeanMapper.map(push, Push.class);
		push.setId("p4");
		push.getTarget().setDeviceId("4");
		push.getTarget().setToken("4");
		pushs.add(push);
		
		push=BeanMapper.map(push, Push.class);
		push.setId("p5");
		push.getTarget().setDeviceId("5");
		push.getTarget().setToken("5");
		push.getTarget().setAppId("app3");
		push.setChannelId("apns");
		pushs.add(push);
		
		push=BeanMapper.map(push, Push.class);
		push.setId("p6");
		push.getTarget().setDeviceId("6");
		push.getTarget().setToken("6");
		pushs.add(push);
		
		return pushs;
	}
	
}
