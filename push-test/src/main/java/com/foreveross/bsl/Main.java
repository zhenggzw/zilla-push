/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.bsl.cmd.OpenfireMsgReceiveCmd;
import com.foreveross.bsl.cmd.OpenfireMsgSendCmd;
import com.foreveross.bsl.cmd.OpenfirePushCmd;
import com.foreveross.bsl.cmd.PrintMessageStatisticsCmd;
import com.foreveross.bsl.cmd.PushRequestCmd;
import com.foreveross.bsl.cmd.QuitCmd;
import com.foreveross.bsl.cmd.ResetMessageStatisticsCmd;
import com.foreveross.bsl.cmd.UserRegisterCmd;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-11-22
 *
 */
public class Main {
	
	final static Logger log = LoggerFactory.getLogger(Main.class);
	private final static String APP_NAME="push-test";
	
	private MessageStatistics msgStats;
	
	Main(){
		msgStats=new MessageStatistics();
	}
	
	public MessageStatistics getMessageStatistics(){
		return this.msgStats;
	}
	
	protected void run(String[] args) throws Exception {
		welcome();
		Command cmd=this.parseStaticCmd(args);
		if(Command.EMPTY_CMD.equals(cmd)){
			return;
		}
		cmd.execute();
		
		//交互命令
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		while(true){
			cmd=this.parseInteractiveCmd(br.readLine());
			cmd.execute();
		}
	}
	
	private void welcome(){
		System.out.println("Welcome to use push-test, have fun!");
		System.out.println("Press 'q' to quit.");
	}
	
	private Command parseInteractiveCmd(String cmdString){
		if("q".equalsIgnoreCase(cmdString)){
			return new QuitCmd();
		}
		else if("p".equalsIgnoreCase(cmdString)){
			return new PrintMessageStatisticsCmd(this.msgStats);
		}
		else if("r".equalsIgnoreCase(cmdString)){
			return new ResetMessageStatisticsCmd(this.msgStats);
		}
		else {
			return Command.EMPTY_CMD;
		}
	}
	
	private Command parseStaticCmd(String[] args) throws Exception{
		Command cmd=Command.EMPTY_CMD;
		if(args.length<=0){
			printUsage();
			return cmd;
		}
		String cmdStr=args[0];
		if("of_usereg".equalsIgnoreCase(cmdStr)){
			if(args.length<2){
				System.out.println("参数不正确，of_usereg命令的参数格式为：{config file} [count] [from]");
				return cmd;
			}
			if(args.length>3){
				return new UserRegisterCmd(new Config(args[1]), Integer.parseInt(args[3]), Integer.parseInt(args[2]));
			}
			return new UserRegisterCmd(new Config(args[1]));
		}
		else if("of_receiver".equalsIgnoreCase(cmdStr)){
			if(args.length<2){
				System.out.println("参数不正确，of_receiver命令的参数格式为：{config file}");
				return cmd;
			}
			cmd=new OpenfireMsgReceiveCmd(this.msgStats, new Config(args[1]));
		}
		else if("of_sender".equalsIgnoreCase(cmdStr)){
			if(args.length<2){
				System.out.println("参数不正确，of_sender命令的参数格式为：{config file}");
				return cmd;
			}
			cmd=new OpenfireMsgSendCmd(new Config(args[1]));
		}
		else if("of_push".equalsIgnoreCase(cmdStr)){
			if(args.length<2){
				System.out.println("参数不正确，of_push命令的参数格式为：{config file}");
				return cmd;
			}
			cmd=new OpenfirePushCmd(this.msgStats, new Config(args[1]));
		}
		else if("push".equalsIgnoreCase(cmdStr)){
			if(args.length<2){
				System.out.println("参数不正确，push命令的参数格式为：{config file} [repeat] [sessionKey]");
				return cmd;
			}
			String sessionKey= args.length>3 ? args[3] : null;
			PushRequestCmd prCmd=new PushRequestCmd(new Config(args[1]), sessionKey);
			if(args.length>2){
				prCmd.setRepeat(Integer.parseInt(args[2]));
			}
			cmd=prCmd;
		}
		return cmd;
	}
	
	private void printUsage(){
		String usage=APP_NAME+" {cmd} [options]\n " +
				"\tcommands: \n" +
				"\tof_usereg:\t注册Openfire用户\n" +
				"\t\t命令格式：of_usereg {config file} [count] [from]\n" +
				"\tof_receiver:\t启动大量Openfire客户端用来压在线用户数\n" +
				"\t\t命令格式：of_receiver {config file}\n" +
				"\tof_sender:\t对客户端模拟发送消息\n" +
				"\t\t命令格式：of_sender {config file}"+
				"\tof_push:\t使用push客户端去openfire签到\n" +
				"\t\t命令格式：of_push {config file}"+
				"\tpush:\t使用push客户端推送消息\n" +
				"\t\t命令格式：push {config file} [repeat] [sessionKey]";
		System.out.println(usage);
	}
	
	public static void main(String[] args) throws Exception {
		new Main().run(args);
	}
}
