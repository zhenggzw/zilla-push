/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-12-6
 * 
 */
public class PushMessage {
	private String id;
	private String title;
	private Date sendTime;
	private Date arrivedTime;
	
	/**
	 * 
	 */
	public PushMessage() {
	}
	
	public PushMessage(String title) {
		this.title=title;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the sendTime
	 */
	@JsonIgnore
	public Date getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime
	 *            the sendTime to set
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return the arrivedTime
	 */
	@JsonIgnore
	public Date getArrivedTime() {
		return arrivedTime;
	}

	/**
	 * @param arrivedTime the arrivedTime to set
	 */
	public void setArrivedTime(Date arrivedTime) {
		this.arrivedTime = arrivedTime;
	}
	
	@JsonIgnore
	public long getArrivedDelay(){
		if(this.arrivedTime!=null && this.sendTime!=null){
			return this.arrivedTime.getTime()-this.sendTime.getTime();
		}
		return Long.MIN_VALUE;
	}

}
