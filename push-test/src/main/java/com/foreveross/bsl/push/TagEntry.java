/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 标签VO
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-9
 *
 */
@XmlRootElement(name = "TagEntry")
public class TagEntry {
	
	private String key;
	private String value;
	
	public TagEntry() {
	}
	
	public TagEntry(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
