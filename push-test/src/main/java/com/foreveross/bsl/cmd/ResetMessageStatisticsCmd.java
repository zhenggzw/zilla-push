/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.cmd;

import com.foreveross.bsl.Command;
import com.foreveross.bsl.MessageStatistics;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-12-6
 *
 */
public class ResetMessageStatisticsCmd implements Command {

	private MessageStatistics msgStats;
	
	public ResetMessageStatisticsCmd(MessageStatistics msgStats){
		this.msgStats=msgStats;
	}

	@Override
	public void execute() {
		this.msgStats.reset();
		System.out.println("消息计数已清除！");
	}

}
