/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.cmd;

import com.foreveross.bsl.Command;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-12-6
 *
 */
public class QuitCmd implements Command {

	@Override
	public void execute() {
		System.exit(0);
	}

}
