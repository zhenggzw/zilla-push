/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-30
 *
 */
public class WebClientExceptionMapper implements ResponseExceptionMapper<Exception> {

	/* (non-Javadoc)
	 * @see org.apache.cxf.jaxrs.client.ResponseExceptionMapper#fromResponse(javax.ws.rs.core.Response)
	 */
	@Override
	public Exception fromResponse(Response r) {
		InputStream is = (InputStream)r.getEntity();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuffer message = new StringBuffer();
        String line = null;
         
        try {
            while((line = br.readLine()) != null) {
                message.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
         
        return new Exception(message.toString());
	}

}
