package com.foreveross.bsl.push.web;

import org.springframework.data.domain.Page;

/**
 * 针对mustache的分页数据模型，适配Page类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-5
 *
 */
public class PageMustache {
	private Boolean hasPreviousPage;
	private Boolean hasNextPage;
	private int current;
	private int prevPage, nextPage;
	private int totalPage;
	private int begin, end;
	private PageIndex[] indexs;

	public class PageIndex {
		private int index = 1;
		private Boolean currentIndex;

		public PageIndex() {
		}

		public PageIndex(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public Boolean getCurrentIndex() {
			this.currentIndex = index == current;
			return currentIndex;
		}

	}

	public PageMustache(Page<?> page, int paginationSize) {
		this.current = page.getNumber() + 1;
		this.begin = Math.max(1, current - paginationSize / 2);
		this.end = Math.min(begin + (paginationSize - 1), page.getTotalPages());
		this.totalPage = page.getTotalPages();
		this.hasPreviousPage = page.hasPreviousPage();
		if (this.hasPreviousPage) {
			this.prevPage = current - 1;
		}
		this.hasNextPage = page.hasNextPage();
		if (this.hasNextPage) {
			this.nextPage = current + 1;
		}
		this.buildIndexs();
	}

	private void buildIndexs() {
		indexs = new PageIndex[end - begin + 1];
		int n=0;
		for (int i = begin; i <= end; i++) {
			indexs[n++] = new PageIndex(i);
		}
	}

	public Boolean getHasPreviousPage() {
		return hasPreviousPage;
	}

	public void setHasPreviousPage(Boolean hasPreviousPage) {
		this.hasPreviousPage = hasPreviousPage;
	}

	public Boolean getHasNextPage() {
		return hasNextPage;
	}

	public void setHasNextPage(Boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getPrevPage() {
		return prevPage;
	}

	public int getNextPage() {
		return nextPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

}
