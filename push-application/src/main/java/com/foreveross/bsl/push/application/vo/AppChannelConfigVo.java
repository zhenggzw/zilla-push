package com.foreveross.bsl.push.application.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AppChannelConfig")
public class AppChannelConfigVo {

  private String appId;

	private Date createTime;

	private ApnsConfigVo apnsConfig;

	private ApnsConfigVo apnsSandboxConfig;

	private OpenfireConfigVo openfireConfig;

	private InstantConfigVo instantConfig; // add by hyw 20140519

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public ApnsConfigVo getApnsConfig() {
		return apnsConfig;
	}

	public void setApnsConfig(ApnsConfigVo apnsConfig) {
		this.apnsConfig = apnsConfig;
	}

	public OpenfireConfigVo getOpenfireConfig() {
		return openfireConfig;
	}

	public void setOpenfireConfig(OpenfireConfigVo openfireConfig) {
		this.openfireConfig = openfireConfig;
	}

	public InstantConfigVo getInstantConfig() {
		return instantConfig;
	}

	public void setInstantConfig(InstantConfigVo instantConfig) {
		this.instantConfig = instantConfig;
	}

	/**
	 * @return the apnsSandboxConfig
	 */
	public ApnsConfigVo getApnsSandboxConfig() {
		return apnsSandboxConfig;
	}

	/**
	 * @param apnsSandboxConfig the apnsSandboxConfig to set
	 */
	public void setApnsSandboxConfig(ApnsConfigVo apnsSandboxConfig) {
		this.apnsSandboxConfig = apnsSandboxConfig;
	}

}