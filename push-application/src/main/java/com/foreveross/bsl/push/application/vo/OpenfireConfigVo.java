package com.foreveross.bsl.push.application.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OpenfireConfig")
public class OpenfireConfigVo {

	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
