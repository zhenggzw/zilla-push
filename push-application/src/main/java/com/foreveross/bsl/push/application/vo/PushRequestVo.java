package com.foreveross.bsl.push.application.vo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;

/**
 * 推送请求VO
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-12
 * 
 */
@XmlRootElement(name = "PushRequest")
public class PushRequestVo {

	private String id;

	private ReceiverTypeEnumVo receiverType;

	private String receiverValue;

	private String appId;
	
	private Map<String, String> tags;

	private MessageVo message;

	private int affectedDevices;

	private Date submitTime;

	private String submitUserId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ReceiverTypeEnumVo getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(ReceiverTypeEnumVo receiverType) {
		this.receiverType = receiverType;
	}

	public String getReceiverValue() {
		return receiverValue;
	}

	public void setReceiverValue(String receiverValue) {
		this.receiverValue = receiverValue;
	}

	public int getAffectedDevices() {
		return affectedDevices;
	}

	public void setAffectedDevices(int affectedDevices) {
		this.affectedDevices = affectedDevices;
	}

	public MessageVo getMessage() {
		return message;
	}

	public void setMessage(MessageVo message) {
		this.message = message;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the tags
	 */
	public Map<String, String> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}
	
	@JsonIgnore
	public boolean getByTags(){
		return ReceiverTypeEnumVo.TAG.equals(this.receiverType);
	}
	
	@JsonIgnore
	public List<TagEntryVo> getTagEntrys(){
		if(this.tags==null){
			return null;
		}
		List<TagEntryVo> entrys=Lists.newArrayList();
		for(Map.Entry<String, String> entry : tags.entrySet()){
			entrys.add(new TagEntryVo(entry.getKey(), entry.getValue()));
		}
		return entrys;
	}

}
