package com.foreveross.bsl.push.application;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.foreveross.bsl.common.utils.web.QuerySpecs;
import com.foreveross.bsl.push.application.vo.PushRequestVo;
import com.foreveross.bsl.push.application.vo.PushVo;

/**
 * 推送管理接口
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-10
 *
 */
@Produces({ MediaType.APPLICATION_JSON})
@Consumes({ MediaType.APPLICATION_JSON})
@Path("pushmgmt")
public interface PushMgmtService {

	@GET
	@Path("push-requests/{pageNumber}/{pageSize}")
	Page<PushRequestVo> findPushRequests(@PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize, @QueryParam("querySpecs") QuerySpecs querySpecs);

	@GET
	@Path("push-requests/user/{userId}/{pageNumber}/{pageSize}")
	Page<PushRequestVo> findPushRequestsByUser(@PathParam("userId") String userId, @PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize);
	
	@GET
	@Path("push-requests/app/{appId}/{pageNumber}/{pageSize}")
	Page<PushRequestVo> findPushRequestsByApp(@PathParam("appId") String appId, @PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize, @QueryParam("querySpecs") QuerySpecs querySpecs);

	@GET
	@Path("pushs/req/{requestId}/{pageNumber}/{pageSize}")
	Page<PushVo> findPushsByRequest(@PathParam("requestId") String requestId, @PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize, @QueryParam("querySpecs") QuerySpecs querySpecs);

	@GET
	@Path("pushs/app/{appId}/{pageNumber}/{pageSize}")
	Page<PushVo> findPushsByApp(@PathParam("appId") String appId, @PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize, @QueryParam("querySpecs") QuerySpecs querySpecs);

	@GET
	@Path("pushs/{pageNumber}/{pageSize}")
	Page<PushVo> findPushs(@PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize, @QueryParam("querySpecs") QuerySpecs querySpecs);
	
}
