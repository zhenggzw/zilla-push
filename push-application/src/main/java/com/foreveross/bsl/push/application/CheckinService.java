/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;

/**
 * 终端设备签到服务接口
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-5
 * 
 */
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Path("checkinservice")
public interface CheckinService {
	/**
	 * 设备签到，新增或者更新签到信息
	 * 
	 * @param deviceCheckin 设备签到信息
	 */
	@PUT
	@Path("/checkins")
	void checkin(DeviceCheckinVo deviceCheckin);

	/**
	 * 更新标签
	 * 
	 * @param deviceId 设备ID,不能为空
	 * @param appId 应用ID,不能为空
	 * @param tags 标签map，如果原标签存在相同key，则覆盖该标签的value，否则新增该标签，为null表示清除标签
	 */
	@PUT
	@Path("/checkins/tags")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	void updateTags(@FormParam("deviceId") String deviceId, @FormParam("appId") String appId,
			@FormParam("tags") Map<String, String> tags, @FormParam("alias") String alias);
	
}
