package com.foreveross.bsl.push.application;

import com.foreveross.bsl.push.application.vo.PushVo;
import com.foreveross.bsl.push.application.vo.PushVo.PushStatusEnum;

public class IllegalPushStatusTransferException extends PushModuleException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2590996644043118274L;
	private final PushVo formerPush;
	private final PushStatusEnum intendStatus;
	
	public IllegalPushStatusTransferException(String msg, PushVo formerPush, PushStatusEnum intendStatus) {
		super(msg);
		this.formerPush=formerPush;
		this.intendStatus=intendStatus;
	}
	
	public IllegalPushStatusTransferException(PushVo formerPush, PushStatusEnum intendStatus) {
		super();
		this.formerPush=formerPush;
		this.intendStatus=intendStatus;
	}

	public PushVo getFormerPush() {
		return formerPush;
	}

	public PushStatusEnum getIntendStatus() {
		return intendStatus;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		String superMsg=super.getMessage();
		StringBuilder sb=new StringBuilder();
		sb.append("push former status:").append(formerPush.getStatus()).append(", intend status:").append(intendStatus);
		if(superMsg!=null)
			sb.append(", ").append(superMsg);
		return sb.toString();
	}
}
