package com.foreveross.bsl.push.application.vo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 推送目标VO
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-10
 *
 */
@XmlRootElement(name = "PushTarget")
public class PushTargetVo {

	private String deviceId;
	private String appId;
	private String token;
	private String alias;
	
	/**
	 * 
	 */
	public PushTargetVo() {
	}

	public PushTargetVo(String deviceId, String appId, String token, String alias) {
		super();
		this.deviceId = deviceId;
		this.appId = appId;
		this.token = token;
		this.alias = alias;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
