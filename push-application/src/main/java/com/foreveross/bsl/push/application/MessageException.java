/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-3
 *
 */
public class MessageException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 819969855882854561L;

	/**
	 * @param message
	 * @param cause
	 */
	public MessageException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public MessageException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public MessageException(Throwable cause) {
		super(cause);
	}

}
