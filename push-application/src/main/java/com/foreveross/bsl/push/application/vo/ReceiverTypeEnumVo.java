package com.foreveross.bsl.push.application.vo;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 消息接收者范围选择枚举类型
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-12
 *
 */
@XmlRootElement(name = "ReceiverTypeEnum")
public enum ReceiverTypeEnumVo {
	/**
	 * 指定的设备。
	 */
	DEVICE_ID(1, "设备"),
	
	/**
	 * 指定的 tag。
	 */
	TAG(2, "标签"),
	
	/**
	 * 指定的 alias。
	 */
	ALIAS(3, "别名"),
	
	/**
	 * 对指定appId的所有用户推送消息。
	 */
	APP_ID(4, "应用");
	
	private final int value;
	private final String description;
	
	private ReceiverTypeEnumVo(final int value, final String description) {
		this.value = value;
		this.description=description;
	}
	
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
}
