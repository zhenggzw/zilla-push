/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.vo;

/**
 * MDM消息类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-2
 *
 */
public class MdmMessage extends MessageVo {
	
	private final static String KEY_DEVICE_ID="deviceId";
	private final static String KEY_MDM="mdm";

	public MdmMessage() {
		super();
	}

	public MdmMessage(String title, String content) {
		super(title, content);
	}
	
	@Override
	public MessageTypeEnum getMessageType() {
		return MessageTypeEnum.MDM;
	}
	
	@Override
	public void setMessageType(MessageTypeEnum messageType) {
		
	}
	
	public String getDeviceId(){
		return (String)this.getExtra(KEY_DEVICE_ID);
	}
	
	public void setDeviceId(String deviceId){
		this.putExtra(KEY_DEVICE_ID, deviceId);
	}
	
	public String getMdm(){
		return (String)this.getExtra(KEY_MDM);
	}
	
	public void setMdm(String mdm){
		this.putExtra(KEY_MDM, mdm);
	}

}
