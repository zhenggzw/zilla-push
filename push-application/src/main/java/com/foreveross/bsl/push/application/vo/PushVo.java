package com.foreveross.bsl.push.application.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 推送VO
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-12
 *
 */
@XmlRootElement(name = "Push")
public class PushVo {

	public enum PushStatusEnum {
		PENDING, SENDING, SENT, OFFLINE, FAILURE, RECEIPT, CANCEL
	}

	private String id;
	
	private PushTargetVo target;

	private MessageVo message;
	
	private String channelId;

	private Date createTime;

	private Date receiptTime;
	
	private PushStatusEnum status;
	
	private int retryCount;
	
	private String lastErrorMsg;
	
	private String requestId;
	
	private String sendId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PushTargetVo getTarget() {
		return target;
	}

	public void setTarget(PushTargetVo target) {
		this.target = target;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReceiptTime() {
		return receiptTime;
	}

	public void setReceiptTime(Date receiptTime) {
		this.receiptTime = receiptTime;
	}

	public PushStatusEnum getStatus() {
		return status;
	}

	public void setStatus(PushStatusEnum status) {
		this.status = status;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public String getLastErrorMsg() {
		return lastErrorMsg;
	}

	public void setLastErrorMsg(String lastErrorMsg) {
		this.lastErrorMsg = lastErrorMsg;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public MessageVo getMessage() {
		return message;
	}

	public void setMessage(MessageVo message) {
		this.message = message;
	}

	public String getSendId() {
		return sendId;
	}

	public void setSendId(String sendId) {
		this.sendId = sendId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	private boolean failure;
	
	@JsonIgnore
	public boolean getFailure(){
		this.failure=PushStatusEnum.FAILURE.equals(this.status);
		return failure;
	}
}
