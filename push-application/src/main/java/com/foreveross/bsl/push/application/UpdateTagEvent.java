/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

import java.util.Map;

/**
 * 标签更新事件
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-15
 * 
 */
public class UpdateTagEvent extends Event {
	private String deviceId;
	private String appId;
	private Map<String, String> tags;
	
	public UpdateTagEvent(String deviceId, String appId, Map<String, String> tags) {
		super();
		this.deviceId = deviceId;
		this.appId = appId;
		this.tags = tags;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId
	 *            the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the tags
	 */
	public Map<String, String> getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

}
