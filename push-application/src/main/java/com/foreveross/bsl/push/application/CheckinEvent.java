/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;


import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;

/**
 * 签到事件
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-10
 * 
 */
public class CheckinEvent extends Event {
	public enum Action {
		Checkin, Checkout
	}

	private Action action;
	private DeviceCheckinVo checkinInfo;

	/**
	 * 
	 */
	public CheckinEvent(DeviceCheckinVo checkinInfo) {
		this.action = Action.Checkin;
		this.checkinInfo = checkinInfo;
	}

	public CheckinEvent(Action action, DeviceCheckinVo checkinInfo) {
		this.action = action;
		this.checkinInfo = checkinInfo;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public DeviceCheckinVo getCheckinInfo() {
		return checkinInfo;
	}

	public void setCheckinInfo(DeviceCheckinVo checkinInfo) {
		this.checkinInfo = checkinInfo;
	}

}
