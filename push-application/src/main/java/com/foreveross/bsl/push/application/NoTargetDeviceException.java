package com.foreveross.bsl.push.application;

import com.foreveross.bsl.push.application.vo.MessageParams;

public class NoTargetDeviceException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1476069024713957572L;

	private final MessageParams messageParams;

	public NoTargetDeviceException(MessageParams messageParams) {
		super();
		this.messageParams=messageParams;
	}

	public NoTargetDeviceException(MessageParams messageParams, String msg) {
		super(msg);
		this.messageParams=messageParams;
	}

	@Override
	public String getMessage() {
		String superMsg = super.getMessage();
		StringBuilder sb=new StringBuilder("沒有找到任何目标设备，条件：");
		sb.append("查找目标类型：").append(messageParams.getReceiverType()).append(", 查找值：")
			.append(this.messageParams.isByTag() ? this.messageParams.getTags() : this.messageParams.getReceiverValue());
		if(superMsg != null && !"".equals(superMsg)){
			sb.append(" ").append(superMsg);
		}
		return sb.toString();
	}
}
