package com.foreveross.bsl.push.application;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.foreveross.bsl.push.application.vo.MessageParams;

/**
 * 推送服务接口
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-11
 *
 */
@Produces({ MediaType.APPLICATION_JSON})
@Consumes({ MediaType.APPLICATION_JSON})
@Path("push")
public interface PushService {
	
	/**
	 * 提交推送请求
	 * @param msgParam
	 * @return 推送请求ID
	 * @throws NoTargetDeviceException
	 */
	@POST
	@Path("notifications")
	@Produces(MediaType.TEXT_PLAIN)
	String submitMessage(MessageParams msgParam) throws PushRequestSubmitException;

}
