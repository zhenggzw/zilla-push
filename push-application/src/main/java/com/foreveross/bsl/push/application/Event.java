/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-4
 *
 */
public class Event implements DsRouterIdentifiable {

	private String dsRouter;

	/**
	 * @return the dsRouter
	 */
	public String getDsRouter() {
		return dsRouter;
	}

	/**
	 * @param dsRouter the dsRouter to set
	 */
	public void setDsRouter(String dsRouter) {
		this.dsRouter = dsRouter;
	}
	
}
