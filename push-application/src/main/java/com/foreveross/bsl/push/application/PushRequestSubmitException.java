/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

/**
 * 推送请求提交异常类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-24
 *
 */
public class PushRequestSubmitException extends PushModuleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6629461511498877260L;

	/**
	 * 
	 */
	public PushRequestSubmitException() {

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PushRequestSubmitException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 */
	public PushRequestSubmitException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public PushRequestSubmitException(Throwable cause) {
		super(cause);

	}

}
