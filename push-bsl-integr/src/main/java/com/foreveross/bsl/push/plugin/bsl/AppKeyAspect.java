/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.plugin.bsl;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;


/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-12
 *
 */
@Aspect
public class AppKeyAspect {
	
	@Inject
	private SystemServicesFacade sysWsFacade;

	@Before("execution(* com.foreveross.bsl.push.application.CheckinService.checkin(..))")
	public void checkinBefore(JoinPoint joinPoint){
		Object[] args=joinPoint.getArgs();
		DeviceCheckinVo dc=(DeviceCheckinVo)args[0];
		if(StringUtils.isEmpty(dc.getAppId())){
			throw new IllegalArgumentException("dc.appId不能为空");
		}
		this.sysWsFacade.setRepositoryRouterByAppKey(dc.getAppId());
	}
	
	@Before("execution(* com.foreveross.bsl.push.application.CheckinService.updateTags(..))")
	public void updateTagsBefore(JoinPoint joinPoint){
		Object[] args=joinPoint.getArgs();
		String appKey=(String)args[1];
		if(StringUtils.isEmpty(appKey)){
			throw new IllegalArgumentException("appKey不能为空");
		}
		this.sysWsFacade.setRepositoryRouterByAppKey(appKey);
	}
	
	@Before("execution(* com.foreveross.bsl.push.application.ReceiptService.receipt(..))")
	public void receiptBefore(JoinPoint joinPoint){
		Object[] args=joinPoint.getArgs();
		String appKey=(String)args[2];
		if(StringUtils.isEmpty(appKey)){
			throw new IllegalArgumentException("appKey不能为空");
		}
		this.sysWsFacade.setRepositoryRouterByAppKey(appKey);
	}
	
	@Before("execution(* com.foreveross.bsl.push.application.PushMessageQueryService.fetchSentAndNoReceiptMessage*(..))")
	public void fetchSentAndNoReceiptMessagesBefore(JoinPoint joinPoint){
		Object[] args=joinPoint.getArgs();
		String appKey=(String)args[1];
		if(StringUtils.isEmpty(appKey)){
			throw new IllegalArgumentException("appKey不能为空");
		}
		this.sysWsFacade.setRepositoryRouterByAppKey(appKey);
	}
	
	@Before("execution(* com.foreveross.bsl.push.application.PushMessageQueryService.get(..))")
	public void getMessageBefore(JoinPoint joinPoint){
		Object[] args=joinPoint.getArgs();
		String appKey=(String)args[0];
		if(StringUtils.isEmpty(appKey)){
			throw new IllegalArgumentException("appKey不能为空");
		}
		this.sysWsFacade.setRepositoryRouterByAppKey(appKey);
	}
}
