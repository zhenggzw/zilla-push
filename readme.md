#Zilla-Push Read Me
##Zilla-Push是什么
Zilla-Push是实现推送通知或消息到各平台终端设备的推送系统。目前支持的终端平台有：IOS和Android两大平台。IOS终端设备使用苹果公司自有的推送系统——APNS，Android终端使用开源系统Openfire。Zilla-Push系统实现设备签到、消息分发、渠道适配、推送状态反馈和查询统计等功能。

##目前的状态
该项目目前还处于孵化期，还没有发布一个比较稳定的版本。现在的开发版本为：1.0.0-SNAPSHOT

##用到的外部服务
Zilla-Push用到的服务器有：

* MongoDB 2.2.3
* Redis 2.6.9
* Openfire 3.8.2

##如何跑起来
###安装编译环境
* JDK1.6+
* Maven3.0.3+

###安装服务器环境
如果你已经有以上服务器环境，可以跳过这一步。
####安装MongoDB
到MongoDB的官网下载2.2.3版本的程序。http://www.mongodb.org/downloads
具体的安装方法请参考：http://docs.mongodb.org/manual/installation/
####安装Redis
到Redis的官网下载Redis2.6.9。http://www.redis.io/download
具体的安装方法请参考下载页的安装指南。
####安装Openfire
到Openfire的官网下载Openfire3.8.2。
http://www.igniterealtime.org/downloads/index.jsp#openfire
Openfire的安装非常，只要解开压缩包即开以跑起来。Openfire提供系统初始化安装向导，只要一步一步按向导的提示操作就可完成安装和配置。

###执行脚本
在执行脚本之前把push-server项目中MongoDB、Redis和Openfire的配置修改成自己环境的配置。

* MongoDB的配置路径：push-server\src\main\resources\META-INF\db\data-mongodb.properties
* Redis的配置路径：push-server\src\main\resources\META-INF\db\data-redis.properties
* Openfire的配置路径：push-server\src\main\resources\META-INF\channel\channel.properties

配置好之后，运行zilla-push目录下的脚本：quick-start.bat（windows环境）或quick-start（linux环境），脚本将编译、打包并安装项目的模块到本地仓库，然后启动服务。

在启动服务后，用浏览器访问地址：http://localhost:8080/push-console

##如何验证
目前还缺少终端设备上使用的客户端程序，我们正在整理中。但你可以使用项目下的push-test程序来模拟验证（只能模拟Openfire渠道的设备）。

1.  在push-console（web入口，ws-client）首页下的“配置管理”页面添加登录到Openfire服务器的用户名和密码。该用户可以到Openfire服务器创建。
2.	使用push-test创建Openfire用户。
	java -jar push-test-1.0.0-SNAPSHOT.jar of_usereg config.properties 100 1
该运行命令将在Openfire服务器上创建100个用户，用户名为“newuser”加索引号，从1开始，直到100，用户密码也是对应的“password”加索引号。config.properties为push-test程序运行时需要的配置文件，可从push-test项目中获得。
3.	运行模拟终端。
	java -jar -Xms512m -Xmx1024m -Xss160k push-test-1.0.0-SNAPSHOT.jar of_push config.properties
该命令将模拟指定数量的（比如100个终端设备，在config.properties设置）终端设备去推送系统中签到，并和Openfire建立连接。
4.	推送消息。
	可以使用push-console的推送页面或pust-test推送消息。
如果使用push-test，则运行如下命令：
java -jar push-test-1.0.0-SNAPSHOT.jar push config.properties
5.	验证消息到达
	当推送消息执行时，可以在控制台页面或模拟接收终端看到有消息到达。在控制台页面可以查询每条推送的状态。当所有消息推送完毕，可以到模拟接收终端键入“P”查询消息的到达和回执情况，即调用OpenFire服务器。

##Zilla-Push的未来
Zilla-Push是一个正在成长的开源项目，它没有过去，只有未来。
我们希望抛砖引玉吸引更多的开源爱好者参与进来，慢慢的培植它，把它变得更强大。